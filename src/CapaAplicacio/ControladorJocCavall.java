package CapaAplicacio;

import java.util.Stack;

import CapaPersistencia.JugadorBBDD;
import CapaPersistencia.LoginBBDD;
import CapaDomini.Jugador;
import CapaDomini.Partida;
import CapaDomini.Taulell;

public class ControladorJocCavall {

	
	private Jugador jugador;
	
	private int tipusConnexio;
	private static final int OFFLINE = 0, ONLINE = 1;

	public ControladorJocCavall() {

		this.jugador = new Jugador("Anonim");
		this.tipusConnexio = OFFLINE;

	}

	

	

	public String getNomJugador() {
		return jugador.getNom();
	}

	public void logarse(String usuari, String password) throws Exception {
		new LoginBBDD().logIn(usuari, password);
	}

	private boolean esPotGuardarBBDD(String nom) {
		return tipusConnexio == ONLINE && !nom.equalsIgnoreCase("Anonim");
	}

	public void nouJugador(String nom) throws Exception {
		if (!nom.equals(
				"Anonim")) { /*
								 * Comprobem si existeix el jugador, sino
								 * existeix en creem un de nou; si existeix
								 */
			tipusConnexio = ONLINE;
		}
		// Si el nom �s "Anonim" no cal fer res
		if (esPotGuardarBBDD(nom)) {
			if (new JugadorBBDD().existeixJugador(nom)) {
				jugador = new JugadorBBDD().findJugador(nom);
		
			} else {
				jugador = new Jugador(nom);
			
				// S'ha d'afegir al jugador la nova partida
			}
		}
	}				//primer s'hauria d'eliminar fora de la base de dades
	public void baixaJugador() throws Exception{
		
		if(new JugadorBBDD().existeixJugador(getNomJugador())){
			
		 new JugadorBBDD().baixaJugadorBBDD(jugador);
		}else{
			throw new Exception("El jugador no existeix");
		}
	}
	
	public void finalitzarJoc() throws Exception { // Si el jugador no existeix
													// l'afegim a la BBDD
		if (esPotGuardarBBDD(getNomJugador())) { // i si ja existeix afegim la
													// informaci� de la partida
													// a la BBDD
			if (!new JugadorBBDD().existeixJugador(getNomJugador())) {
				new JugadorBBDD().insertJugador(jugador);
			} else {
				new JugadorBBDD().inserirPartidaBBDD(jugador);
			}
		}
		new LoginBBDD().closeConnection();
	}

	public boolean respostaTipusJoc(int seleccio) { // Decidim si es jugara
													// online o no en la
													// pantallasaltcavall
		if (seleccio == 0)
			this.tipusConnexio = this.OFFLINE;
		else
			this.tipusConnexio = this.ONLINE;
		return this.tipusConnexio == this.OFFLINE;
	}

}
