package CapaPresentacio;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import CapaAplicacio.ControladorJocCavall;

import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PantallaLogin extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JPasswordField passwordField;
	private JTextField textField;
	private JLabel lblUser;
	private JLabel lblPassword;
	private JButton btnContinue;
	private JButton btnPlayOffline;
	private ControladorJocCavall ctl= new ControladorJocCavall();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaLogin frame = new PantallaLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PantallaLogin() {
		initComponents();
	}
	private void initComponents() {
		setTitle("Login");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Alex Haro Medina\\Documents\\Workspaces\\Universidad\\Grup2Practica1LabSoft\\Images\\icono.jpg"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 439, 242);
		this.contentPane = new JPanel();
		this.contentPane.setBackground(SystemColor.inactiveCaption);
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(null);
		
		this.passwordField = new JPasswordField();
		this.passwordField.setBounds(223, 96, 121, 20);
		this.contentPane.add(this.passwordField);
		
		this.textField = new JTextField();
		this.textField.setBounds(223, 45, 121, 20);
		this.contentPane.add(this.textField);
		this.textField.setColumns(10);
		
		this.lblUser = new JLabel("User:");
		this.lblUser.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.lblUser.setBounds(78, 48, 77, 14);
		this.contentPane.add(this.lblUser);
		
		this.lblPassword = new JLabel("Password:");
		this.lblPassword.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.lblPassword.setBounds(78, 99, 77, 14);
		this.contentPane.add(this.lblPassword);
		
		this.btnContinue = new JButton("Continue");
		this.btnContinue.addActionListener(this);
		this.btnContinue.setForeground(Color.WHITE);
		this.btnContinue.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.btnContinue.setBackground(Color.BLACK);
		this.btnContinue.setBounds(66, 154, 110, 23);
		this.contentPane.add(this.btnContinue);
		
		this.btnPlayOffline = new JButton("Play Offline");
		this.btnPlayOffline.addActionListener(this);
		this.btnPlayOffline.setForeground(Color.WHITE);
		this.btnPlayOffline.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.btnPlayOffline.setBackground(Color.BLACK);
		this.btnPlayOffline.setBounds(234, 154, 110, 23);
		this.contentPane.add(this.btnPlayOffline);
	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == this.btnContinue) {
			btnContinueActionPerformed(arg0);
		}
		if (arg0.getSource() == this.btnPlayOffline) {
			btnPlayOfflineActionPerformed(arg0);
		}
	}
	protected void btnPlayOfflineActionPerformed(ActionEvent arg0) {
		this.setVisible(false);
	}
	protected void btnContinueActionPerformed(ActionEvent arg0) {
		boolean continuar = true;
		String password = new String(passwordField.getPassword());
		try{
			ctl.logarse(textField.getText(), password);
		}
		catch(Exception e){
			continuar = showOptionPlayOffline(e.getMessage());
		}
		if (continuar) {
			new PantallaSaltCavall();
			this.dispose();
		}
	}
	private boolean showOptionPlayOffline(String message){
		JOptionPane.showMessageDialog(this, message, "Error Login",
				JOptionPane.ERROR_MESSAGE);
		int seleccio = JOptionPane.showOptionDialog(this,
				"Vols jugar sense connexi� a la BBDD", "Error Login",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				null, new Object[] { "SI", "NO" }, "SI");
		return ctl.respostaTipusJoc(seleccio);
	}
}
