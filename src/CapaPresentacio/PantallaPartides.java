package CapaPresentacio;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.Font;
import java.awt.Color;
import java.awt.Window.Type;
import java.awt.Toolkit;

public class PantallaPartides extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JCheckBox checkBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaPartides frame = new PantallaPartides();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PantallaPartides() {
		initComponents();
		this.setVisible(true);
	}
	
	int i =10;
	int p = 1;
	int y = 10;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JButton btnDeleteSelectedGames;
	private JButton btnDeleteAllGames;
	private JButton btnClose;
	private void initComponents() {
		setType(Type.POPUP);
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Alex Haro Medina\\Documents\\Workspaces\\Universidad\\Grup2Practica1LabSoft\\Images\\icono.jpg"));
		setTitle("Partides");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 613, 341);
		this.contentPane = new JPanel();
		this.contentPane.setBackground(SystemColor.inactiveCaption);
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(this.contentPane);
		this.contentPane.setLayout(null);
		
		this.scrollPane = new JScrollPane();
		this.scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.scrollPane.setBounds(10, 11, 378, 280);
		
		this.panel = new JPanel();
		this.scrollPane.setViewportView(this.panel);
		this.panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
	
		this.contentPane.add(this.scrollPane);
		
		
		while(p<=i){
			this.checkBox = new JCheckBox("Partida " + p + " " + new Date().toString());
			this.checkBox.setBounds(10, y, 300, 23);
			this.scrollPane.add(this.checkBox);
			p++;
			y += 25;
			}
		
			
		
		this.btnDeleteSelectedGames = new JButton("Delete selected games");
		this.btnDeleteSelectedGames.addActionListener(this);
		this.btnDeleteSelectedGames.setBackground(Color.BLACK);
		this.btnDeleteSelectedGames.setForeground(Color.WHITE);
		this.btnDeleteSelectedGames.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.btnDeleteSelectedGames.setBounds(398, 97, 178, 23);
		this.contentPane.add(this.btnDeleteSelectedGames);
		
		this.btnDeleteAllGames = new JButton("Delete all games");
		this.btnDeleteAllGames.setBackground(Color.BLACK);
		this.btnDeleteAllGames.setForeground(Color.WHITE);
		this.btnDeleteAllGames.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.btnDeleteAllGames.addActionListener(this);
		this.btnDeleteAllGames.setBounds(398, 131, 178, 23);
		this.contentPane.add(this.btnDeleteAllGames);
		
		this.btnClose = new JButton("Close");
		this.btnClose.setBackground(Color.BLACK);
		this.btnClose.setForeground(Color.WHITE);
		this.btnClose.setFont(new Font("Tahoma", Font.BOLD, 12));
		this.btnClose.addActionListener(this);
		this.btnClose.setBounds(398, 165, 178, 23);
		this.contentPane.add(this.btnClose);
	}
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == this.btnDeleteSelectedGames) {
			btnDeleteSelectedGamesActionPerformed(arg0);
		}
		if (arg0.getSource() == this.btnDeleteAllGames) {
			btnDeleteAllGamesActionPerformed(arg0);
		}
		if (arg0.getSource() == this.btnClose) {
			btnCloseActionPerformed(arg0);
		}
	}
	
	
	protected void btnDeleteSelectedGamesActionPerformed(ActionEvent arg0) {
		int n = JOptionPane.showConfirmDialog(
			    this,
			    "Would you like to delete the selected games?",
			    "Delete games",
			    JOptionPane.YES_NO_OPTION);
		//System.out.println("delete selected" + n);
		//n = 0 -> Option SI
		//n = 2 -> Option NO

	}
	protected void btnDeleteAllGamesActionPerformed(ActionEvent arg0) {
		int n = JOptionPane.showConfirmDialog(
			    this,
			    "Would you like to delete all games?",
			    "Delete all games",
			    JOptionPane.YES_NO_OPTION);
	}
	protected void btnCloseActionPerformed(ActionEvent arg0) {
		this.setVisible(false);
	}
}
