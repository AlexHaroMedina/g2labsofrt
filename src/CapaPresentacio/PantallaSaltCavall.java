package CapaPresentacio;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import CapaAplicacio.ControladorJocCavall;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenu;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Toolkit;

@SuppressWarnings("serial")
public class PantallaSaltCavall extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton btnDesferMoviment;
	private JButton reinicia;
	private static JPanel taulell;
	private JPanel menu;
	private JButton[][] botonsDeJoc;
	private ControladorJocCavall joc;
	private JTextField violacionsDeJoc;
	private static int mida = 5;
	private JMenuBar menuBar;
	private JMenu mnOpcions;
	private JMenuItem mntmPartides;
	private JMenuItem mntmExit;
	private PantallaPartides pantallaPartides;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaSaltCavall frame = new PantallaSaltCavall();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		});
	}

	public PantallaSaltCavall() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Alex Haro Medina\\Documents\\Workspaces\\Universidad\\Grup2Practica1LabSoft\\Images\\icono.jpg"));
		setTitle("Salt del cavall");
		initComponents();
		this.setVisible(true);
	}

	private void initComponents() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 500);
		
		this.mida=midaTaulell();
		
		this.menuBar = new JMenuBar();
		setJMenuBar(this.menuBar);
		
		this.mnOpcions = new JMenu("Opcions");
		this.mnOpcions.setFont(new Font("Segoe UI", Font.BOLD, 12));
		this.menuBar.add(this.mnOpcions);
		
		this.mntmPartides = new JMenuItem("Partides");
		this.mntmPartides.addActionListener(this);
		this.mnOpcions.add(this.mntmPartides);
		
		this.mntmExit = new JMenuItem("Exit");
		this.mnOpcions.add(this.mntmExit);
		this.contentPane = new JPanel();
		setContentPane(this.contentPane);
		this.contentPane.setLayout(new BorderLayout(0, 0));
		this.joc = new ControladorJocCavall(PantallaSaltCavall.mida);
		this.btnDesferMoviment = new JButton("Desfer moviment");
		this.reinicia = new JButton("Reinicia");
		this.btnDesferMoviment.addActionListener(this);
		this.reinicia.addActionListener(this);
		PantallaSaltCavall.taulell = new JPanel();
		this.menu = new JPanel();
		this.contentPane.add(PantallaSaltCavall.taulell, BorderLayout.CENTER);
		this.contentPane.add(this.menu, BorderLayout.SOUTH);
		PantallaSaltCavall.taulell.setLayout(new GridLayout(PantallaSaltCavall.mida, PantallaSaltCavall.mida));
		this.menu.setLayout(new GridLayout(2,1));
		this.menu.add(this.btnDesferMoviment);
		this.menu.add(this.reinicia);
		this.botonsDeJoc = new JButton[PantallaSaltCavall.mida][PantallaSaltCavall.mida];
		this.violacionsDeJoc = new JTextField();
		this.contentPane.add(this.violacionsDeJoc, BorderLayout.NORTH);

		for (int i = 0; i < PantallaSaltCavall.mida; i++) {
			for (int j = 0; j < PantallaSaltCavall.mida; j++) {
				this.botonsDeJoc[i][j] = new JButton();
				PantallaSaltCavall.taulell.add(this.botonsDeJoc[i][j]);
				this.pintarBoto(i,j);

				this.botonsDeJoc[i][j].addActionListener(this);
			}
		}

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.mntmPartides) {
			mntmPartidesActionPerformed(e);
		}
		this.violacionsDeJoc.setText("");
		if (e.getSource() == this.btnDesferMoviment) {
			try {
				int[] coordenades = this.joc.desferMoviment();
				this.botonsDeJoc[coordenades[0]][coordenades[1]].setText(null);
				this.possiblesMoviments();
			} catch (Exception ex) {
				this.violacionsDeJoc.setText(ex.getMessage());
			}
		} else {
			if (e.getSource() == this.reinicia) {
				this.reiniciaJoc();
			} else {
				for (int i = 0; i < PantallaSaltCavall.mida; i++) {
					for (int j = 0; j < PantallaSaltCavall.mida; j++) {
						if (e.getSource() == this.botonsDeJoc[i][j]) {
							try {
								joc.rebreCoordenades(i, j);
								this.botonsDeJoc[i][j].setText(Integer.toString(joc.getContador()));
								this.possiblesMoviments();
							} catch (Exception e1) {
								this.violacionsDeJoc.setText(e1.getMessage());
							}
						}
					}
				}
			}
		}

		finalitzaJoc();
	}

	private void finalitzaJoc() {
		if (joc.jocFinalitzat()) {
			int n = JOptionPane.showConfirmDialog(taulell, "Vol reiniciar el joc?", "HA GUANYAT!!!",
					JOptionPane.YES_NO_OPTION);
			if (n == JOptionPane.YES_OPTION) {
				reiniciaJoc();
			} else {
				//System.exit(0);
			}

		}
	}

	private void reiniciaJoc() {
		for (int i = 0; i < PantallaSaltCavall.mida; i++) {
			for (int j = 0; j < PantallaSaltCavall.mida; j++) {
				this.botonsDeJoc[i][j].setText(null);
				this.pintarBoto(i,j);
			}
		}
		this.joc.reiniciarJoc();
	}
	
	private static int midaTaulell(){
		int n;
		Object[] possibilities = {"3", "4", "5", "6", "7", "8", "9", "10"};
		String s = (String)JOptionPane.showInputDialog(taulell,
		                    "Escollir la mida del taulell \n",
		                    "MIDA TAULELL",
		                    JOptionPane.PLAIN_MESSAGE,
		                    null, possibilities,
		                    "3");
		int a;

		//If a string was returned, say so.
	
		return n=Integer.parseInt(s);
		//If you're here, the return value was null/empty.
	
	}
	
	
	
	
	
	private void possiblesMoviments(){
		for(int i=0; i<PantallaSaltCavall.mida;i++){
			for(int j=0; j<PantallaSaltCavall.mida; j++){
				if(this.botonsDeJoc[i][j].getBackground() == Color.GREEN){
					this.pintarBoto(i,j);
				}
			}
		}
		Stack<Integer> [] casellesBuides = this.joc.possiblesMoviments();
		while(!casellesBuides[0].empty() && !casellesBuides[1].empty()){
			this.botonsDeJoc[casellesBuides[0].pop()][casellesBuides[1].pop()].setBackground(Color.GREEN);
		}
	}
	
	private void pintarBoto(int x, int y){
		if ((x + y) % 2 != 0) {
			this.botonsDeJoc[x][y].setBackground(Color.WHITE);
			this.botonsDeJoc[x][y].setForeground(Color.BLACK);

		} else {
			this.botonsDeJoc[x][y].setBackground(Color.BLACK);
			this.botonsDeJoc[x][y].setForeground(Color.WHITE);
			;
		}
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
	protected void mntmPartidesActionPerformed(ActionEvent e) {
		this.pantallaPartides = new PantallaPartides();
	}
}
