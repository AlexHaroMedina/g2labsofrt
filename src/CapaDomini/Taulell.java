package CapaDomini;

public class Taulell {
	
	//Taulell normal que cont� objectes casella
	
	private Casella [] [] taulell;
	private final int BUIDA = 0;
	 
	public Taulell(int mida){
		this.taulell = new Casella [mida][mida];
		this.inicialitzarTaulell();
	}
	

	
	private void inicialitzarTaulell(){
	
		for (int i = 0; i < this.taulell.length; i++) {
			for (int o = 0; o < this.taulell[0].length; o++) {
				this.taulell[i][o] = new Casella(BUIDA);//creem les caselles buides
			}
		}
		
	}
	
	public boolean casellaBuida(int x, int y)throws Exception{
		try{
		return (taulell[x][y].getContingut()== BUIDA);
		}catch(IndexOutOfBoundsException e){
			throw new Exception("Les coordenades introdu�des estan fora del taulell");
		}
	}
	
	public void buidarCasella(int x, int y){
		 this.taulell[x][y].setContingut(BUIDA);
	}
	
	

	public void posicionarFitxa (int x, int y, int contingut){
		this.taulell[x][y].setContingut(contingut);
	}

	public void reiniciarTaulell(){
		this.inicialitzarTaulell();
	}
	
	public String toString(){ //Return matriu d'Strings
		String impresioTaulell = "";
		
		for (int i = 0; i < this.taulell.length; i++) {
			for (int o = 0; o < this.taulell[0].length; o++) {
				impresioTaulell = impresioTaulell + this.taulell[i][o].toString() + "\t";
			}
			impresioTaulell = impresioTaulell + "\n";
		}
		
		return impresioTaulell;
	}
	


}
