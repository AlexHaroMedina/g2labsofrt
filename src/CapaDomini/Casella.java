package CapaDomini;

public class Casella {
	private int contingut;

	public Casella(int contingut){
		this.contingut = contingut;
	}
	
	public int getContingut(){
		return this.contingut;
	}
	
	public void setContingut(int contingut) {
		this.contingut = contingut;
	}
	
		
	public String toString(){
		return Integer.toString(this.contingut);
	}

}
