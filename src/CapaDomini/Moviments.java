package CapaDomini;


import java.util.Stack;


public class Moviments {
	  
	  private Stack<int []> coordenades;
	  
	  
	  public Moviments(){
	    this.coordenades = new Stack<int []>();

	  }
	  
	  public void afegir(int x, int y){
	    this.coordenades.push(new int[] {x,y});
	  }
	    
	  public int [] obtenir() throws Exception{
	    if (!coordenades.empty()){
	      return this.coordenades.pop();
	    }
	    else{
	      throw new Exception ("No hi ha moviments per desfer.");
	    }
	  }
	    
	  public int [] consulta(){
	    return this.coordenades.peek();  
	  }
	  
	  
	  public void resetejarMoviments(){
	    this.coordenades.clear();
	  }
	}
