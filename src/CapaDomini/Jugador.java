package CapaDomini;

import java.util.ArrayList;
import java.util.List;

public class Jugador {


	 private String nom;
	    private Partida darreraPartida;
	    private ArrayList<Partida> partides;
	    private int guanyades = 0;
	    


	    public Jugador(String nom) {  //Constructor Jugador
	        this.nom = nom;
	        guanyades = 0;
	        partides = new ArrayList<Partida>();

	    }

	    public String getNom() {  //Obtenir el nom del jugador
	        return nom;
	    }

	    public void setNom(String nom) {  //Canviar el nom del jugador
	        this.nom = nom;
	    }
	    public String resultatPartides() {  //Obtenir les partides en string(s'ha de fer un to string de la partida)
	        String resultat = "";
	        for (Partida p : partides) {
	            resultat += p + "\n";
	        }
	        return resultat;
	    }

	    public void addPartida(int mida) {  //afegir la darrera partida a la llista de partides del jugador
	        darreraPartida = new Partida(mida);
	        this.desarDarreraPartida();
	    
	    }
	    public void removePartida(){
	    	this.partides.remove(nombrePartides());
	    }

	    public String resultatDarreraPartida() {  //Obtenir resultat de la darrera partida
	        return darreraPartida.toString();
	    }

	    public int nombrePartides() {  //Nombre de partides de la llista del jugador
	        return partides.size();
	    }
	    /*public boolean guanyadaDarreraPartida() {   //S'ha de comprobar perque nosaltres el getGuanyada el tenim en el Controlador
	        return darreraPartida.getGuanyada();
	    }
	    private void actualitzarGuanyades() {
	        guanyades += darreraPartida.getGuanyada() ? 1 : 0;
	    }

	    public int nombreGuanyades() {
	        return guanyades;
	    }*/

	    private void desarDarreraPartida() {  //Afegir la ultima partida a la llista
	        partides.add(darreraPartida);
	    }


	    public List<Partida> getPartides() {  //Obtenir totes les partides del jugador
	        return partides;
	    }

	  
	    public Partida getPartidaNoGuardada() {  //Obtenir la ultima partida de la llista per guardarla al jugador en la classe PartidaBBDD
			return partides.get(partides.size()-1);
		}
}
