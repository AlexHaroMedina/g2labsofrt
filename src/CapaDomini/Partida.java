package CapaDomini;

import java.util.Stack;

public class Partida {

	private Moviments moviments;
	private Taulell taulell;
	private int numMaxMoviments;
	private int midaTaulell;
	private int contador;

	public Partida(int mida) {
		this.taulell = new Taulell(mida);
		this.numMaxMoviments = (int) Math.pow(mida, 2);
		moviments = new Moviments();
		this.midaTaulell = mida;
		contador = 0;

	}

	public int getContador() {
		return this.contador;

	}

	public void rebreCoordenades(int x, int y) throws Exception {
		if (taulell.casellaBuida(x, y)) {
			this.moviment(x, y);
			this.moviments.afegir(x, y);
			this.contador++;
			moureFitxa(x, y, this.contador);
		} else {
			throw new Exception("Ja ha passat per aquesta casella anteriorment");
		}
	}

	private void moureFitxa(int x, int y, int contador) {
		this.taulell.posicionarFitxa(x, y, contador);
	}

	public int[] desferMoviment() throws Exception {
		int[] retorn = this.moviments.obtenir();
		this.taulell.buidarCasella(retorn[0], retorn[1]);
		this.contador--;
		return retorn;
	}

	// M�tode per comprovar si el moviment de cavall �s correcte.
	private void moviment(int x, int y) throws Exception {
		if (this.contador > 0) {
			int[] coordenades = this.moviments.consulta();
			if (Math.abs(coordenades[0] - x) == 2 && Math.abs(coordenades[1] - y) == 1
					|| Math.abs(coordenades[0] - x) == 1 && Math.abs(coordenades[1] - y) == 2) {

			} else {
				throw new Exception("Moviment de cavall incorrecte");
			}
		}
	}

	public void reiniciarJoc() {

		this.taulell.reiniciarTaulell();
		this.moviments.resetejarMoviments();
	}

	public String mostrarTaulell() {
		return this.taulell.toString();
	}

	public Stack<Integer>[] possiblesMoviments() {
		Stack<Integer> x = new Stack<Integer>();
		Stack<Integer> y = new Stack<Integer>();
		for (int i = 0; i < this.midaTaulell; i++) {
			for (int j = 0; j < this.midaTaulell; j++) {
				try {
					if (this.taulell.casellaBuida(i, j)) {
						this.moviment(i, j);
						x.add(i);
						y.add(j);
					}
				} catch (Exception e) {
				}
			}
		}
		Stack<Integer> casellesBuides[] = new Stack[2];
		casellesBuides[0] = x;
		casellesBuides[1] = y;
		return casellesBuides;
	}

	public boolean jocFinalitzat() {
		return (this.contador == this.numMaxMoviments);
	}

	public int getMidaTaulell() {
		return this.midaTaulell;
	}

}
