package CapaPersistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import CapaDomini.Jugador;
import CapaDomini.Partida;

public class PartidaBBDD {
	private ConnectionBBDD connexio;
	

	public PartidaBBDD() throws Exception { // Constructor on obtenim la

											// connexi�

		// connexi�

		connexio = ConnectionContainer.getConnection();
		
	}

	void inserirPartidaBBDD(Jugador jugador) throws Exception { // //Afegim a la

																// base de dades
																// una partida

		// base de dades
		// una partida

		try {
			Partida partidaNoGuardada = jugador.getPartidaNoGuardada();
			PreparedStatement insertQuery = createInsertQuery(jugador);

			prepareInsertQuery(insertQuery, partidaNoGuardada);
			insertQuery.executeUpdate();

		} catch (SQLException e) {
			throw new Exception("Error al inserir partides");
		}
	}



	private PreparedStatement createInsertQuery(Jugador jugador)
			throws SQLException { // En

		String sql = "INSERT INTO PARTIDAGUARDADA(IDPARTIDA, NOM, RESULTAT, HORAJUGADA, MIDATAULELL) VALUES(?,?,?,?,?)";
		PreparedStatement insertPartidaStatement = connexio
				.prepareStatement(sql);
		insertPartidaStatement.clearParameters();
		insertPartidaStatement.setString(2, jugador.getNom());
		return insertPartidaStatement;
	}
	

	
	void omplirPartidesJugador(Jugador jugador) throws SQLException{
		ResultSet result = executeSelectPartides(jugador);
		while(result.next()){
			int mida= result.getInt("midataulell");
			jugador.addPartida(mida);
		}
	
	}
	
	private ResultSet executeSelectPartides(Jugador jugador) throws SQLException{
		String sql = "SELECT * FROM PARTIDAGUARDADA WHERE NOMJUGADOR = ?";
		PreparedStatement statement = connexio.prepareStatement(sql);
		statement.clearParameters();
		statement.setString(2, jugador.getNom());
		return statement.executeQuery();
	}

	public void eliminarPartidesBBDD(Jugador jugador) throws Exception {
		try{
		
			PreparedStatement insertQuery = eliminarInsertQuery(jugador);
			insertQuery.executeUpdate();
		}
	
	catch(SQLException e){
		throw new Exception("Error al inserir partides");
	}

		
	}

	private void prepareInsertQuery(PreparedStatement insertQuery,
			Partida partida) throws Exception {
		int partidaId = createPartidaId();
		insertQuery.setInt(1, partidaId);

		insertQuery.setInt(3, partida.getMidaTaulell()); // Resultat i mida
		// taulell esta en
		// el controlador,
		// la hora jugada s'haura d'obtenir encara
		insertQuery.setBoolean(4, partida.jocFinalitzat());
		//insertQuery.setInt(5, partida.getHora()); // Els gets s�n exemples nom�s
	}

	private PreparedStatement createPKValueQuery() throws SQLException {
		String sql = "SELECT S_IDPARTIDA.NEXTVAL PKPARTIDA FROM DUAL";
		return connexio.prepareStatement(sql); // Mirar amb atenci� o preguntar
		// que fa exactament
	}

	private int createPartidaId() throws Exception {
		ResultSet generatedKey = createPKValueQuery().executeQuery();
		if (generatedKey.next())
			return generatedKey.getInt(1);
		throw new Exception("Invalid PK");
	}

	void omplirPartidaJugador(Jugador jugador, int mida) throws SQLException { // Omplim
																				// la
																				// darrera
																				// partida
																				// feta
																				// al
																				// jugador

		jugador.addPartida(mida);

	}


private PreparedStatement eliminarInsertQuery(Jugador jugador) throws SQLException{
	String sql = "DELETE FROM PARTIDAGUARDADA WHERE NOM=?";
	PreparedStatement insertPartidaStatement = connexio.prepareStatement(sql);
	insertPartidaStatement.clearParameters();
	insertPartidaStatement.setString(2, jugador.getNom());
	return insertPartidaStatement;
}
}
