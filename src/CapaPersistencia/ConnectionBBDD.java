package CapaPersistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.CallableStatement;

public class ConnectionBBDD extends AbsConnexioBBDD {

	private final String urlConnection = "jdbc:oracle:thin:@rama.eupmt.es:1521:LOGO10";
	private Connection connexio;

	public ConnectionBBDD(String usuari, String password) throws Exception {

		try {
			DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
			connexio = DriverManager.getConnection(urlConnection, usuari,
					password);
		} catch (Exception e) {
			throw new Exception("Error en la conexio Oracle" + e.getMessage());
		}
	}
		public PreparedStatement prepareStatement(String sql) throws SQLException {
			return connexio.prepareStatement(sql);
		}
		
		public CallableStatement prepareCall(String sql) throws SQLException {
			return connexio.prepareCall(sql);
		}
		public void close() throws SQLException {
			connexio.close();
		}
		public boolean isClosed() throws SQLException {
			return connexio.isClosed();
		}
	
}
