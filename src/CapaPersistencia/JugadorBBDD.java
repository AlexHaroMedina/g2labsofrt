package CapaPersistencia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import CapaDomini.Jugador;

public class JugadorBBDD {

	private ConnectionBBDD connexio;
	private PartidaBBDD partidaBBDD;

	public JugadorBBDD() throws Exception {
		connexio = ConnectionContainer.getConnection();
		partidaBBDD = new PartidaBBDD();
	}

	public boolean existeixJugador(String nom) throws Exception {
		// Comprovem si el nom del jugador entrat existeix

		try {
			ResultSet result = executeSelectJugador(nom);
			return result.next();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception("Error al buscar jugador.");
		}
	}

	private Jugador getJugadorBBDD(String nom) throws Exception {
		// Retornem el jugador en que afegirem la partida
		ResultSet result = executeSelectJugador(nom);
		if (!result.next())
			throw new Exception("El jugador no existeix");
		String nomJugador = result.getString(1);
		return new Jugador(nomJugador);

	}

	private ResultSet executeSelectJugador(String nom) throws SQLException {
		// busquem en quin jugador afegirem la partida
		String sql = "SELECT NOMJUGADOR FROM JUGADOR WHERE NOMJUGADOR=?";
		PreparedStatement selectStatement = connexio.prepareStatement(sql);
		selectStatement.clearParameters();
		selectStatement.setString(1, nom);
		return selectStatement.executeQuery();
	}

	public void inserirPartidaBBDD(Jugador jugador) throws Exception {
		try {
			if (existeixJugador(jugador.getNom())) {
				partidaBBDD.inserirPartidaBBDD(jugador);
			} else {
				throw new Exception("El jugador no existeix");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception("Error al inserir partida");
		}
	}

	public void insertJugador(Jugador jugador) throws Exception {
		// Comprobem que no hi hagi hagut cap problema al inserir el jugador

		boolean correct = insertJugadorBBDD(jugador);
		if (!correct)
			throw new Exception("No s'ha afegit el jugador");
		inserirPartidaBBDD(jugador);

	}

	private boolean insertJugadorBBDD(Jugador jugador) throws SQLException {
		// Inserim al jugador a la BBDD
		String sql = "INSERT INTO JUGADOR(NOM) VALUES(?)";
		PreparedStatement insertJugador = connexio.prepareStatement(sql);
		insertJugador.clearParameters();
		insertJugador.setString(1, jugador.getNom());
		return insertJugador.executeUpdate() == 1;
	}

	public Jugador findJugador(String nom) throws Exception {
		// Omplim la partida en el jugador
		try {
			Jugador jugador = getJugadorBBDD(nom);
			partidaBBDD.omplirPartidesJugador(jugador);
			return jugador;
		} catch (SQLException e) {
			throw new Exception("Error al buscar jugador");
		}
	}

	public void baixaJugadorBBDD(Jugador jugador) throws Exception {
		// Comprobem que no hi hagi hagut cap problema al inserir el jugador7

		eliminarPartidesBBDD(jugador);
		boolean correct = eliminarJugadorBBDD(jugador);
		if (!correct)
			throw new Exception("No s'ha eliminat el jugador");
		eliminarPartidesBBDD(jugador);

	}

	private boolean eliminarJugadorBBDD(Jugador jugador) throws Exception {
		String sql = "DELETE FROM JUGADOR WHERE NOM=?";
		PreparedStatement esborrarJugador = connexio.prepareStatement(sql);
		esborrarJugador.clearParameters();
		esborrarJugador.setString(1, jugador.getNom());
		return esborrarJugador.executeUpdate() == 1;

	}

	private void eliminarPartidesBBDD(Jugador jugador) throws Exception {

		if (existeixJugador(jugador.getNom())) {
			partidaBBDD.eliminarPartidesBBDD(jugador);

		}
	}
}
