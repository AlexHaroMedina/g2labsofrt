package CapaPersistencia;

import java.sql.SQLException;

public class ConnectionContainer {

	private static ConnectionBBDD connectionBBDD;

	public static ConnectionBBDD getConnection() throws Exception {

		if (connectionBBDD == null) {
			throw new Exception("Not init");
		}
		return connectionBBDD;

	}

	static void iniConnection(String usuari, String password) throws Exception {
		if (connectionBBDD == null) {
			connectionBBDD = new ConnectionBBDD(usuari, password);
		}
	}

	public static void closeConnection() throws Exception {
		try {
			connectionBBDD.close();
			connectionBBDD = null;
		} catch (SQLException e) {
			throw new Exception("No s'ha tancat la connexi�");
		}
	}

}
